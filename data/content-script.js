// stolen from somewhere
!function(d,s,id){
    var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';
    if(!d.getElementById(id)){
        js=d.createElement(s);
        js.id=id;
        js.src=p+"://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js,fjs);
    }
}(document,"script","twitter-wjs");

var bookmarks = document.querySelectorAll('.bookmark');

for (var i = 0, bookmark_count = bookmarks.length; i < bookmark_count; i++) {
    var bookmark = bookmarks[i];
    var tags = bookmark.querySelectorAll('.tag');

    for (var j = 0, tag_count = tags.length; j < tag_count; j++) {
        var tag = tags[j].textContent;

        if (!tag.match(/^via:https?:\/\//)) {
            continue;
        }

        var bq = document.createElement('blockquote');
        bq.className = 'twitter-tweet';

        var href = tag.substr(4);
        var newLink = document.createElement('a');
        newLink.href = href;
        newLink.textContent = href;
        newLink.target = '_blank';

        bq.appendChild(newLink);
        bookmark.appendChild(document.createElement('br'));
        bookmark.appendChild(bq);
    }
}

unsafeWindow.twttr.widgets.load();
